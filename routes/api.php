<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'v1'], function () {
    // JWT tokens
    Route::post('authenticate', 'Auth\AuthenticateController@authenticate');
    Route::get('token', 'Auth\AuthenticateController@token');
    // Product listings
    Route::resource('products', 'ProductController', 
        ['only' => ['index', 'show'] 
    ]);
});

