<?php

namespace App\Http\Controllers\Auth;

use JWTAuth;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Exceptions\TokenInvalidException;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use App\Http\Controllers\Controller;

class AuthenticateController extends Controller
{
    /**
     * Authenticate login credentials and return a JWT token upon success
     * @param  Request $request 
     * @return string
     */
    public function authenticate(Request $request)
    {
        // grab credentials from the request
        $credentials = $request->only('email', 'password');

        try {
            // attempt to verify the credentials and create a token for the user
            if (! $token = JWTAuth::attempt($credentials)) {
                return response()->json(['error' => 'invalid_credentials'], 401);
            }
        } catch (JWTException $e) {
            // something went wrong while attempting to encode the token
            return response()->json(['error' => 'could_not_create_token'], 500);
        }

        // all good so return the token
        return response()->json(compact('token'));
    }  

    /**
     * Returns a new JWT token provided a valid one
     * @param  Request $request 
     * @return string           
     */
    public function token(Request $request)
    {
        $token = JWTAuth::getToken();
        if (!$token){
            return response()->json(['error' => 'token_not_provided'], 400);
        }
        $token = JWTAuth::refresh($token);

        return response()->json(compact('token'));
    }
}
