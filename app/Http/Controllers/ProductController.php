<?php

namespace App\Http\Controllers;

use JWTAuth;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    /**
     * Set the jwt.auth middleware on this controller
     */
    public function __construct()
    {
        $this->middleware(['jwt.auth']);
    }

    /**
     * Display a listing of the resource.
     * @todo  Still testing JWT
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // testing stuff...
        $user = JWTAuth::parseToken()->authenticate();
        $payload = JWTAuth::parseToken()->getPayload();

        return response()->json(['message' => $payload]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }
}
