<?php

namespace App\Listeners;

use App\Events\ProductUrlAddedEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class ProductUrlAddedNotification
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ProductUrlAddedEvent  $event
     * @return void
     */
    public function handle(ProductUrlAddedEvent $event)
    {
        $url     = $event->getUrl();
        $scraper = $event->getScraper();
        $scraper->updateProductDetails($url);
    }
}
