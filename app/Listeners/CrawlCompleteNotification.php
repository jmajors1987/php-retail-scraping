<?php

namespace App\Listeners;

use App\User;
use App\Events\HasBeenCrawled;
use App\Notifications\CrawlComplete;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Log;

class CrawlCompleteNotification
{
    /**
     * Create the event listener.
     * Leaving for now for if I want to inject something via the service container
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     * @todo  We will inject a Mailable/Notification to send off to a queue
     * @param  HasBeenCrawled  $event
     * @return void
     */
    public function handle(HasBeenCrawled $event)
    {
        $crawler = $event->getCrawler();
        $url     = $crawler->getBaseUrl();
        // Notification here
        //User::where('name', '=', 'Jason Majors')->first()->notify((new CrawlComplete($crawler))->onQueue('emails'));
        Log::info("CRAWLED $url");
    }
}
