<?php
namespace App\Services\Listings;

use App\Product;
use App\Listing;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Log;

abstract class AbstractListings
{
    /**
     * Get the listing data for the marketplace
     * @todo  Move Client out of here. The child class can implement their own HTTP client
     * @param  Client $client An instance of GuzzleHttp\Client
     * @param  string $upc    The Universal Product Code    
     * @return array          An array containing an array for each UPC listing. 
     *                        Keys: 'title', 'lowest_price', 'total_for_sale', 'offers', 'marketplace'
     */
    abstract public function getListingData(Client $client, $upc);

    /**
     * Iterates over all the products and checks if they are available on the implemented marketplace
     * @param  array $stores Optional parameter to only check products of certain stores
     * @return [type]        [description]
     */
    public function checkProducts(array $stores=[])
    {
        $client = new Client();
        Product::chunk(1000, function ($products) use ($stores, $client) {
            foreach ($products as $product) {
                // If we've specified specific stores AND the product did not come from that store...
                if (!empty($stores) && !in_array($product->store, $stores)) {
                    continue;
                }
                // check UPC here and get listing data. Could make this a job being dispatched on a delay
                $listings = $this->getListingData($client, $product->upc->code);
                // Continue to the next product if no valid response from API
                if (!$listings) {
                    continue;
                }
                foreach ($listings as $listing) {
                    $title          = $listing['title'];
                    $lowest_price   = $listing['lowest_price'];
                    $total_for_sale = $listing['total_for_sale'];
                    $marketplace    = $listing['marketplace'];
                    $upc_id         = $product->upc->id;

                    $identifiers = compact('title', 'upc_id', 'marketplace');
                    $attributes  = compact('lowest_price', 'total_for_sale');
                    // If listing exists, update the attributes
                    Log::info("Updating listing data for $title");
                    Listing::updateOrCreate($identifiers, $attributes);
                }
            }
        });
    }
}