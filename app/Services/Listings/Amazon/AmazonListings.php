<?php

namespace App\Services\Listings\Amazon;

use Exception;
use SimpleXMLElement;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Response;
use GuzzleHttp\Exception\RequestException;
use App\Services\Listings\AbstractListings;
use Illuminate\Support\Facades\Log;

/**
 * Wrap the Amazon Product Availability API to meet the AbstractListings contract
 */
class AmazonListings extends AbstractListings
{
    const END_POINT  = "webservices.amazon.com"; 
    const XML_URI    = "/onca/xml";

    /**
     * Generates the GET request URL
     * @param  string $upc 12 character numeric UPC
     * @link   http://webservices.amazon.com/scratchpad/index.htm
     * @return string      
     */
    private function prepareRequest($upc)
    {
        $params = [
            "Service"        => "AWSECommerceService",
            "Operation"      => "ItemLookup",
            "AWSAccessKeyId" => env('AMAZON_KEY_ID'),
            "AssociateTag"   => env('AMAZON_ASSOC_ID'),
            "ItemId"         => $upc,
            "IdType"         => "UPC",
            "ResponseGroup"  => "ItemAttributes,Offers",
            "SearchIndex"    => "All",
            "Timestamp"      => gmdate('Y-m-d\TH:i:s\Z')
        ];
        // Sort the parameters by key
        ksort($params);
        $pairs = [];
        // Begin building querystring
        foreach ($params as $key => $value) {
            array_push($pairs, rawurlencode($key)."=".rawurlencode($value));
        }
        // Generate the querystring
        $querystring = join("&", $pairs);
        // Generate the string to be signed
        $string_to_sign = "GET\n".self::END_POINT."\n".self::XML_URI."\n".$querystring;
        // Generate the signature required by the Product Advertising API
        $signature = base64_encode(hash_hmac("sha256", $string_to_sign, env('AMAZON_SECRET_ACCESS_KEY'), true));
        // Generate the signed URL
        $requestUri = 'http://'.self::END_POINT.self::XML_URI.'?'.$querystring.'&Signature='.rawurlencode($signature);

        return $requestUri;
    }
    
    /**
     * Checks if the API returned a valid HTTP response
     * @param  Response $response A GuzzleHttp response
     * @param  string   $upc      Universal product code
     * @return boolean            Whether or not the API returns a valid response
     */
    private function isValidResponse(Response $response, $upc)
    {
        $valid = true;
        $statusCode = $response->getStatusCode();
        if ($statusCode !== 200) {
            Log::info("Request returned $statusCode for UPC $upc");
            // No data to return
            $valid = false;
        }

        return $valid;
    }

    /**
     * Return array of data needed keyed by marketplace_listings columns
     * @param  Client $cient      An instance of GuzzleHttp\Client
     * @param  string $upc 12 character numeric UPC
     * @return array An array containing an array for each UPC listing
     */
    public function getListingData(Client $client, $upc)
    {
        $listingData  = [];
        $uri          = $this->prepareRequest($upc);
        try {
            $response = $client->request('GET', $uri, ['delay' => 1010]);
        } catch (RequestException $e) {
            $error = $e->getMessage();
            Log::info("Problem with HTTP request for listing $upc. Error: $error");
            
            return $listingData;
        }
        // Check that we get a valid 200 response from the API
        if ($this->isValidResponse($response, $upc)) {  
            $responseBody = (string) $response->getBody();
            $listings  = new SimpleXMLElement($responseBody);
            // Check for error in valid http response from API e.g. invalid key or UPC
            if (isset($listings->Items->Request->Errors->Error->Code)) {
                $errorCode = $listings->Items->Request->Errors->Error->Code;
                // Amazon doesn't like this UPC for some reason
                Log::info("Amazon API returned an error: $errorCode for UPC $upc");
                $listingData = false;
            }

            // WHY ARE THERE SOMETIMES MULTIPLE ITMES FOR A SINGLE UPC - WHAT THE HELL BEZOS
            foreach ($listings->Items->Item as $item) {
                $title          = (string) $item->ItemAttributes->Title;
                $lowest_price   = (float) ($item->OfferSummary->LowestNewPrice->Amount / 100); // API returns price in cents
                $currencyCode   = (string) $item->OfferSummary->LowestNewPrice->CurrencyCode;
                $total_for_sale = (int) $item->OfferSummary->TotalNew;
                $offers         = (string) $item->Offers->TotalOffers;
                $marketplace    = 'amazon';

                if ($currencyCode != "USD") {
                    // Not messing with products from other countries at the moment...
                    $listingData = false;
                }  else {
                    $listingData[] = compact('title', 'lowest_price', 'total_for_sale', 'offers', 'marketplace');
                } 
            }
        }

        return $listingData;
    }
}