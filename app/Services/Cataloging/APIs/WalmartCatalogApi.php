<?php
namespace App\Services\Cataloging\APIs;

use App\Product;
use App\Exceptions\ProductAttributeException;
use App\UPC;
use App\Jobs\UpdateProductsByCategoryFromApi;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Psr7\Response;
use Illuminate\Support\Facades\Log;

class WalmartCatalogApi implements CatalogInterface, CreatesFromCategoryInterface
{
	const BASE   = 'http://api.walmartlabs.com/v1/';
	const FORMAT = 'json';
    // USD
    const FREE_SHIPPING_PRICE = 35;
	
    /**
	 * API parameters for makeRequest()
	 * @var array
	 */
	private $params;

	/**
	 * Set the params
	 */
    public function __construct()
    {
    	$this->params = [
    		"format" => self::FORMAT,
    		"apiKey" => config('spidering.walmart_api.api_key')
    	];
    }

    /**
     * Checks if the API returned a valid HTTP response
     * @param  Response $response A GuzzleHttp response
     * @return boolean            Whether or not the ApPI returns a valid response
     */
    private function isValidResponse(Response $response)
    {
        $valid = true;
        $statusCode = $response->getStatusCode();
        if ($statusCode !== 200) {
            Log::info("Walmart API request returned $statusCode");
            // No data to return
            $valid = false;
        }

        return $valid;
    }

    /**
     * Makes a call to the Walmart Open API
     *
     * @see    https://developer.walmartlabs.com/io-docs
     * @param  string $type The API method
     * @param  array $params
     * @return \Psr\Http\Message\ResponseInterface
     */
	private function makeRequest($type, $params=[])
	{
		$params		 = array_merge($this->params, $params);
        $querystring = http_build_query($params);

		$uri = self::BASE . "{$type}?{$querystring}";
        $response = (new Client)->request('GET', $uri, ['delay' => 205]); // A little over 5 calls per second
        // Return false on bad response
        if (!$this->isValidResponse($response)) {
        	$response = false;
        }

        return $response;
	}

	// get all categories
	private function getCategoryIds()
	{
		$response = $this->makeRequest('taxonomy');
		// If valid response, iterate over the categories and build an array of their IDs
		if ($response) {
			$categories  = json_decode($response->getBody(), true);
			$categories  = $categories['categories'];
			$categoryIds = $this->findCategoryCode($categories);

			return $categoryIds;
		}
	}
	
	/**
	 * Recursively finds the most specific category code
	 * @param  array  $categories 
	 * @return array             
	 */
	private function findCategoryCode(array $categories)
	{
		static $categoryIds = [];

		foreach ($categories as $category) {
			if (isset($category['children'])) {
				$this->findCategoryCode($category['children']);
			} else {
				$categoryIds[] = $category['id'];
			}
		}

		return $categoryIds;
	}

	/**
	 * Updates the Product data for a given category
	 * @param  int $categoryId 
	 * @return void
	 */
	public function updateProductData($categoryId)
	{
        $response = $this->makeRequest('paginated/items', ['category' => $categoryId]);
		// If valid response, iterate over the categories and build an array of their IDs
		if ($response) {
			$category = json_decode($response->getBody(), true);
			$items = isset($category['items']) ? $category['items'] : [];
			foreach($items as $item) {
				// Some items/walmart products dont have UPCs in the response, and we won't be able to work with these
				if (!isset($item['upc'])) {
					continue;
				}
        		$product = $this->makeProductFromResponse($item);
        		if ($product) {
					$upc = UPC::firstOrCreate(['code' => $item['upc']]);
        			$upc->products()->save($product);
                    // TESTING - return after 1 product saved
                    if (\App::environment('testing')) {
                        return;
                    }
                    // END TESTING
        		}
			}
		}
	}
	
	/**
	 * Creates a Product model from the item response data from Walmart
	 * @param  array  $item An array of the item data from the Walmart response
	 * @return Product      The Product instance
	 */
	private function makeProductFromResponse(array $item)
	{
		$marketplace = isset($item['marketplace']) ? $item['marketplace'] : '';
		$available   = isset($item['availableOnline']) ? $item['availableOnline'] : false;
		$stock 		 = $item['stock'];
		$store 		 = 'walmart';
		$title 		 = $item['name'];
		$category    = isset($item['categoryPath']) ? $item['categoryPath'] : '';
		$num_reviews = isset($item['numReviews']) ? $item['numReviews'] : 0;
		$description = isset($item['longDescription']) ? $item['longDescription'] : false;
		$description = iconv('ASCII', 'UTF-8//IGNORE', $description);
		$images 	 = json_encode([
			'largeImage'  => isset($item['largeImage']) ? $item['largeImage'] : '', 
			'mediumImage' => isset($item['mediumImage']) ? $item['mediumImage'] : '', 
			'smallImage'  => isset($item['thumbnailImage']) ? $item['thumbnailImage'] : ''
		]);
		$clearance = isset($item['clearance']) ? $item['clearance'] : false;
		$rollback  = isset($item['rollback']) ? $item['rollback'] : false;
		$on_sale   = ($clearance || $rollback) ? true : false;
		$url = isset($item['productUrl']) ? $item['productUrl'] : '';
		// TODO: Need to figure out what's going on with prices...
		if (isset($item['salePrice'])) {
			$price = $item['salePrice'];
		} elseif (isset($item['msrp'])) {
			$price = $item['msrp'];
		} else {
			$price = false;
		}
        // Check if this is a product we can use
        $valid = $this->validate(compact('price', 'stock', 'available', 'marketplace'));
        if ($valid['valid']) {
            $productData  = compact('store', 'title', 'category', 'num_reviews', 'on_sale', 'description', 'images', 'url');
            $currentPrice = compact('price');
            // Create product if new, else update with current price
            $product = Product::updateOrCreate($productData, $currentPrice);
            $errors  = $product->hasErrors();
            if ($errors) {
                // Log errors rather than throwing exception. The API implementation populates products from the catalog as 1 large job
                // Don't want to trigger job failure on validation isssues
                Log::info(ProductAttributeException::makeErrorMessage($product));
                $product = false;
            }
        } else {
            $product = false;
            $errors  = join(', ', $valid['errors']);
            Log::info("Not creating entry for $title from $url: $errors");
        } 

		return $product;
	}

	/**
	 * Validate that the product is available online from Walmart
	 * @todo  Move to separate Validator class
	 * @param  array  $attributes 
	 * @return array
	 */
	private function validate(array $attributes)
	{
		$valid    = true;
		$errors = [];
        // should add if price over $35
		array_walk($attributes, function(&$value, $key) use (&$valid, &$errors) {
			switch ($key) {
                case 'price':
                    if ($value < self::FREE_SHIPPING_PRICE) {
                        $valid = false;
                        $errors[] = "Does not meet free shipping requirements";
                    }
                    break;
				case 'stock':
					if ($value == 'unavailable') {
						$valid = false;
						$errors[] = "Stock unavailable";
					}
					break;
				case 'available':
					if ($value === false) {
						$valid = false;
						$errors[] = "Not available online";
					}
					break;
				case 'marketplace':
					if ($value === true) {
						$valid = false;
						$errors[] = "Only available via marketplace";
					}
					break;
				default:
					break;
			}
		});
		return compact('valid', 'errors');
	}

	/**
	 * Run through all the categories and update the catalog
	 * @return void
	 */
	public function updateCatalog()
	{
		$categoryIds = $this->getCategoryIds();
		foreach ($categoryIds as $categoryId) {
            // Dispatch job to get product data 
            dispatch((new UpdateProductsByCategoryFromApi(new self, $categoryId))->onQueue('products'));
			 // Testing purposes - safe to remove
            if (\App::environment('local', 'testing')) {
                return;
            }
            // END TESTING
		}
	}	
}