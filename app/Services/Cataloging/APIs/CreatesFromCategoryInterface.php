<?php
namespace App\Services\Cataloging\APIs;

interface CreatesFromCategoryInterface
{
    /**
     * Creates Products from a categoryId
     * @param  int  $categoryId 
     * @return void
     */
    public function updateProductData($categoryId);
}

