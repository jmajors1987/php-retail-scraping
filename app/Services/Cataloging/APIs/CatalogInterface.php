<?php
namespace App\Services\Cataloging\APIs;

interface CatalogInterface
{
    /**
     * Updates the store's product catalog
     * @return void
     */
	public function updateCatalog();    
}