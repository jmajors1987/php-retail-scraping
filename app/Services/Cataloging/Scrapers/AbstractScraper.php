<?php
namespace App\Services\Cataloging\Scrapers;

use App\Exceptions\ProductAttributeException;
use App\Product;
use App\UPC;
use App\Services\Cataloging\Scrapers\AbstractScraper;
use Goutte\Client;
use Symfony\Component\DomCrawler\Crawler;
use Illuminate\Support\Facades\Log;

abstract class AbstractScraper
{
    /**
     * The store title
     * @var string
     */
    protected $store;

    /**
     * Retrieve the title of a product from a product page
     * @param  Crawler $crawler
     * @return string           
     */
    abstract protected function getProductTitle(Crawler $crawler);

    /**
     * Retrieve the price of a product from a product page
     * @param  Crawler $crawler
     * @return string           
     */
    abstract protected function getProductPrice(Crawler $crawler);

    /**
     * Retrieve the description of a product from a product page
     * @param  Crawler $crawler
     * @return string           
     */
    abstract protected function getProductDesc(Crawler $crawler);

    /**
     * Checks if a product is either unavailable to ship, or unavailable to add to cart
     * @param  Crawler $crawler 
     * @return boolean
     */
    abstract protected function getAvailability(Crawler $crawler);

    /**
     * Retrieve how many customer reviews the product has
     * @param  Crawler $crawler
     * @return string           
     */
    abstract protected function getReviewCount(Crawler $crawler);
    
    /**
     * Retrieve whether or not a product is on sale
     * @param  Crawler $crawler
     * @return boolean
     */
    abstract protected function isOnSale(Crawler $crawler);

    /**
     * Retrieve the image of a product from a product page
     * @param  Crawler $crawler
     * @return string           
     */
    abstract protected function getProductImages(Crawler $crawler);
    
    /**
     * Scrapes the UPC (Universal Product Code) for the product
     * @param  Crawler $crawler 
     * @return int           
     */
    abstract protected function getProductUpc(Crawler $crawler);

    /**
     * Make a DomCrawler instance in the child class that will work for the intended site
     * @param  string $url The URL of the page to scrape
     * @return Symfony\Component\DomCrawler\Crawler
     */
    abstract protected function getCrawler($url);

    /**
     * Set the store name and the product URL table for the scraper
     */
    public function __construct()
    {
        $this->store = $this->getStoreName();
    }

    /**
     * Insert/update the product data for a given page
     * @todo    No need anymore to have the DomCrawler instance in here for all the getters. Can just live in the implementation
     * @param   string $url The product page URL
     * @throws  ProductAttributeException
     * @return void
     */
    final public function updateProductDetails($url)
    {       
        $crawler = $this->getCrawler($url);
        if (!$this->isAvailable($crawler)) {
            Log::info("This product does not meet the availability criteria: $url");
            return;
        }
        // Get the data
        $store       = $this->store; // This is really just so we can use compact() below
        $title       = $this->getProductTitle($crawler);
        $price       = $this->getProductPrice($crawler);
        $description = $this->getProductDesc($crawler);
        $num_reviews = $this->getReviewCount($crawler);
        $on_sale     = $this->isOnSale($crawler);
        $images      = $this->getProductImages($crawler);
        $upc         = $this->getProductUpc($crawler);
        // Retrieve the UPC or create it if it doesnt exist yet
        $upc    = UPC::firstOrCreate(['code' => $upc]);
        $upc_id = $upc->id;
  
        $productData  = compact('store', 'title', 'description', 'num_reviews', 'on_sale', 'images', 'url', 'upc_id');
        $currentPrice = compact('price');
        // If product exists, set the price to $price, else add the new product
        $product = Product::updateOrCreate($productData, $currentPrice);
        $errors  = $product->hasErrors();
        if ($errors) {
            throw new ProductAttributeException($product);
        }
    }

    /**
     * Returns whether or not this product is available for shipping and online purchase
     * @param  Crawler $crawler
     * @return boolean          
     */
    protected function isAvailable(Crawler $crawler)
    {
        return $this->getAvailability($crawler);
    }

    /**
     * Gets the name of the store the current scraper is for
     * @return [type] Lowercase name of the store based off the Scraper class name. E.g WalmartScraper becomes walmart.
     */
    protected function getStoreName()
    {
        $class     = (new \ReflectionClass($this))->getShortName();
        $storeName = strtolower(rtrim($class, 'Scraper'));

        return $storeName;
    }
}