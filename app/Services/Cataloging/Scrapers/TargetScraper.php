<?php
namespace App\Services\Cataloging\Scrapers;

use Exception;
use InvalidArgumentException;
use App\Services\Cataloging\Scrapers\AbstractScraper;
use Symfony\Component\DomCrawler\Crawler;
use App\Services\Cataloging\Traits\HeadlessBrowser;

/**
 * Scrapes target.com product pages
 * @link  https://symfony.com/doc/current/components/dom_crawler.html
 * @return array An array containing the product properties
 */
class TargetScraper extends AbstractScraper
{
    use HeadlessBrowser;

    /*
     * These are the "paths" to their respective dom elements for the Crawler
     */
    const TITLE_DOM   = 'h2.title-product > span';
    const PRICE_DOM   = 'div.price > span';
    const AVAILABLE   = 'div#pdpAtc';
    const DESC_SHORT  = 'section.subnav--content > div#tab-content-details > div';
    const REVIEWS_DOM = 'span.ratings--count > span';
    const IMG_URL     = 'img[itemprop=image]';
    const SALE        = 'span.price--saveStory';
    const UPC         = 'section.subnav--content > div#tab-content-details > ul';
    // Dollar amount (USD)
    const FREE_SHIPPING_PRICE = 25;

    /**
     * The URL being scraped
     * @var string
     */
    private $url;

    /**
     * Makes a Crawler instance for the Target scraper implementaion.
     * @param  string $url
     * @return Symfony\Component\DomCrawler\Crawler
     */
    public function getCrawler($url)
    {
        $this->url = $url;
        // HeadlessBrowser method
        $html    = $this->getGeneratedHTML($url);
        $crawler = new Crawler($html);

        return $crawler;
    }

    /**
     * Scrapes the product title
     * 
     * @param  Crawler
     * @return string The title of the product
     */
    protected function getProductTitle(Crawler $crawler)
    {
        try {
            $title = $crawler->filter(self::TITLE_DOM)->text();
        } catch (InvalidArgumentException $e) {
            $title = false;
        }

        return $title;
    }

    /**
     * Scrapes the product price
     * @todo  A small amount of product pages have the price listed as a range. I need to decide how to handle that case
     * @param  Crawler
     * @return float
     */
    protected function getProductPrice(Crawler $crawler)
    {
        try {
            $price = ltrim(trim($crawler->filter(self::PRICE_DOM)->text()), '$');
        } catch (InvalidArgumentException $e) {
            $price = false;
        }

        return $price;
    }

    /**
     * Checks if a product is either unavailable to ship, or unavailable to add to cart
     * @param  Crawler $crawler 
     * @return boolean
     */
    protected function getAvailability(Crawler $crawler)
    {
        $available    = true;
        $price        = $this->getProductPrice($crawler);
        $checkoutText = $this->checkoutPanel($crawler);
        if (preg_match('/(not eligible|unavailable|out of stock)/', $checkoutText)) {
            $available = false;
        }
        // not eligible, out of stock, unavailable
        // If the product does not quailfy for free shipping, or doesn't have a shipping option, or doesn't have an online checkout option...
        if ($price < self::FREE_SHIPPING_PRICE) {
            $available = false;
        }

        return $available;
    }


    /**
     * Scrapes the checkout panel text
     * @todo  This still randomly fails sometimes in job, where it works fine in console???
     * @param  Crawler $crawler
     * @return string           
     */
    protected function checkoutPanel(Crawler $crawler)
    {
        try {
            $checkoutText = trim($crawler->filter(self::AVAILABLE)->text());
        } catch (InvalidArgumentException $e) {
            throw new Exception("Unable to verify checkout text: $this->url");
        }

        return $checkoutText;
    }

    /**
     * Scrapes the product description
     * 
     * @param  Crawler 
     * @return string The product description
     */
    protected function getProductDesc(Crawler $crawler)
    {
        try {
            $description = $crawler->filter(self::DESC_SHORT)->text();
        } catch (InvalidArgumentException $e) {
            $description = false;
        }

        return $description;
    }

    /**
     * Scrapes the product review count
     * @param  Crawler $crawler 
     * @return int
     */
    protected function getReviewCount(Crawler $crawler)
    {
        try {
            $reviews = (int) $crawler->filter(self::REVIEWS_DOM)->text();
        } catch (InvalidArgumentException $e) {
            $reviews = false;
        }

        return $reviews;
    }

    /**
     * Checks whether or not the product is on sale
     * @param  Crawler $crawler 
     * @return boolean          
     */
    protected function isOnSale(Crawler $crawler)
    {
        try {
            $salesText = $crawler->filter(self::SALE)->text();
            $onSale    = trim($salesText) != '' ? true : false;
        } catch (InvalidArgumentException $e) {
            $onSale = false;
        }

        return $onSale;
    }

    /**
     * Scrapes the product images
     * 
     * @param Crawler $crawler
     * @return string The img src path
     */
    protected function getProductImages(Crawler $crawler)
    {
        try {
            $image = $crawler->filter(self::IMG_URL)->attr('src');
        } catch (InvalidArgumentException $e) {
            $image = 'No images found';
        }
        
        return $image;
    }

    /**
     * Scrapes the UPC (Universal Product Code) for the product
     * @param  Crawler $crawler 
     * @return int           
     */
    protected function getProductUpc(Crawler $crawler)
    {
        $upc = false;
        // Not always present...
        try {
            // Get the list of product attributes, explode on UPC, then parse the actual content we want
            $content = explode('UPC:', $crawler->filter(self::UPC)->text());
            // Check if explode found 'UPC:'
            if (count($content) > 1) {
                $upc = substr(trim($content[1]), 0, 12);            
            } 
        // We'll throw our own Exception later if theres an invalid UPC
        } catch (InvalidArgumentException $e) {}

        return $upc;
    }
}