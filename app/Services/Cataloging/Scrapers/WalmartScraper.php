<?php
namespace App\Services\Cataloging\Scrapers;

use InvalidArgumentException;
use App\Services\Cataloging\Scrapers\AbstractScraper;
use Goutte\Client;
use Symfony\Component\DomCrawler\Crawler;

/**
 * Scrapes walmart.com product pages
 * @todo  Add validation for each product attribute being scraped
 * @link https://symfony.com/doc/current/components/dom_crawler.html
 * @return array An array containing the product properties
 */
class WalmartScraper extends AbstractScraper
{
    /*
    Dont change these
     */
    const TITLE_DOM   = 'h1.prod-ProductTitle > div';
    const PRICE_DOM   = 'span[itemprop=price]';
    const DESC_SHORT  = 'div.about-desc[data-tl-id=AboutThis-ShortDescription]';
    const SALE        = 'span.Price-save-text';
    const REVIEWS_DOM = 'span.stars-reviews-count-node';
    const IMG_URL     = 'div.prod-HeroImage-container > img';
    const UPC         = 'span[itemprop=gtin13]';
    // Dollar amount (USD)
    const FREE_SHIPPING_PRICE = 35;

    protected function getCrawler($url)
    {
        $client = new Client();
        $client->setHeader('user-agent', env('USER_AGENT'));
        $crawler = $client->request('GET', $url);

        return $crawler;
    }

    /**
     * Scrapes the product title
     * 
     * @param  Crawler
     * @return string The title of the product
     */
    protected function getProductTitle(Crawler $crawler)
    {
        $titleNode = $crawler->filter(self::TITLE_DOM)->first();
        // If this node exists, add the text to the array
        if ($titleNode->count()) {
            return $titleNode->text();
        }
    }

    /**
     * Scrapes the product price
     * 
     * @param  Crawler
     * @return float
     */
    protected function getProductPrice(Crawler $crawler)
    {
        try {
            $price = $crawler->filter(self::PRICE_DOM)->attr('content');
        } catch (InvalidArgumentException $e) {
            $price = false;
        }

        return $price;
    }

    /**
     * Scrapes the product description
     * 
     * @param  Crawler 
     * @return string The product description
     */
    protected function getProductDesc(Crawler $crawler)
    {
        try {
            $description = $crawler->filter(self::DESC_SHORT)->last()->text();
        } catch (InvalidArgumentException $e) {
            $description = false;
        }

        return $description;
    }

    /**
     * Checks if a product is either unavailable to ship, or unavailable to add to cart
     * @todo  IMPLEMENT THIS: NEED TO CHECK CHECKOUT PANEL INFO
     * @param  Crawler $crawler 
     * @return boolean
     */
    protected function getAvailability(Crawler $crawler)
    {
        // TODO: need to check the checkout panel
        $available    = true;
        $price        = $this->getProductPrice($crawler);
        if ($price < self::FREE_SHIPPING_PRICE) {
            $available = false;
        }

        return $available;
    }

    /**
     * Scrapes the product review count
     * @param  Crawler $crawler 
     * @return int
     */
    protected function getReviewCount(Crawler $crawler)
    {
        try {
            $reviewText = $crawler->filter(self::REVIEWS_DOM)->text();
            $reviews    = (int) rtrim($reviewText, ' reviews');
        } catch (InvalidArgumentException $e) {
            $reviews = false;
        }

        return $reviews;
    }

    /**
     * Checks whether or not the product is on sale
     * @param  Crawler $crawler 
     * @return boolean          
     */
    protected function isOnSale(Crawler $crawler)
    {
        try {
            $salesText = $crawler->filter(self::SALE)->text();
            $onSale    = trim($salesText) != '' ? true : false;
        } catch (InvalidArgumentException $e) {
            $onSale = false;
        }

        return $onSale;
    }

    /**
     * Scrapes the product images
     * 
     * @param Crawler $crawler
     * @return array The img src paths
     */
    protected function getProductImages(Crawler $crawler)
    {
        try {
            $image = $crawler->filter(self::IMG_URL)->attr('src');
        } catch (InvalidArgumentException $e) {
            $image = false;
        }

        return $image;
    }

    /**
     * Scrapes the UPC (Universal Product Code) for the product
     * @param  Crawler $crawler 
     * @return int           
     */
    protected function getProductUpc(Crawler $crawler)
    {
        try {
            $upc = $crawler->filter(self::UPC)->attr('content');
            // Remove unneeded trailing 0s if present
            $upc = substr($upc, -12);
            // Make sure we have a valid UPC
            if (!is_numeric($upc)) {
                $upc = false;
            } 
        } catch (InvalidArgumentException $e) {
            $upc = false;
        }

        return $upc;
    }
}