<?php
namespace App\Services\Cataloging\Crawlers;

use App\Services\Cataloging\Crawlers\AbstractCrawler;
use Illuminate\Support\Facades\Log;
use Robots\Exceptions\MissingRobotsTxtException;

class WalmartCrawler extends AbstractCrawler
{
    // Walmart has an API implementation that is much faster than scraping, so for now the sitemap implementation has not been built out
    public static $useSitemap = false;

    public function getBaseUrl()
    {
        return 'https://www.walmart.com';
    }

    /**
     * Check if a URL is a Walmart product page
     * @param  string  $url
     * @return boolean
     */
    protected function isProductPage($url)
    {
        // check if '/ip' is part of the string (these are the product pages)
        return (strpos($url, '/ip/') !== false);    
    }
    
    /**
     * Generator for product page URLs from the sitemap
     * @param  array  $sitemap
     * @return 
     */
    protected function getProductPageFromSitemap(array $sitemap)
    {
        return false;
    }

    /**
     * Returns the product page sitemap from the sitemap array
     * @param  array  $sitemaps The array of 
     * @return string           The URL for the product page sitemap
     */
    protected function getProductPageMap(array $sitemaps)
    {
        return $sitemaps[0];
    }

    /**
     * Check if the discovered URL is a URL we want to visit
     * @param  string  $url
     * @return boolean
     */
    protected function isRelevantUrl($url)
    {
        $relevant = false;
        $host = parse_url($url)['host'];
        if ($host == 'www.walmart.com' || $host == 'walmart.com') {
            $relevant = true;
        }

        return $relevant;
    }
}