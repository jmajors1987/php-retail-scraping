<?php
namespace App\Services\Cataloging\Crawlers;

use SimpleXMLElement;
use App\Services\Cataloging\Crawlers\AbstractCrawler;
use App\Services\Cataloging\Traits\HeadlessBrowser;
use Illuminate\Support\Facades\Log;
use Robots\Exceptions\MissingRobotsTxtException;
use GuzzleHttp\Client;

class TargetCrawler extends AbstractCrawler
{    
    use HeadlessBrowser; // For if we want to crawl without the sitemap

    public static $useSitemap = true;

    public function getBaseUrl()
    {
        return 'http://www.target.com';
    }

    /**
     * Check if a URL is a Walmart product page
     * @param  string  $url
     * @return boolean
     */
    protected function isProductPage($url)
    {
        // check if '/p/' is part of the string (these are the product pages)
        return (strpos($url, '/p/') !== false);    
    }

    /**
     * Check if the discovered URL is a URL we want to visit
     * @param  string  $url
     * @return boolean
     */
    protected function isRelevantUrl($url)
    {
        $relevant = false;
        $host = parse_url($url)['host'];
        if ($host == 'www.target.com' || $host == 'target.com') {
            $relevant = true;
        }

        return $relevant;
    }

    /**
     * Generator for product page URLs from the sitemap
     * @param  array  $sitemap The low level sitemap containing URLs to the XML files where the product page URLs are
     * @return string          Yields a product page URL
     */
    protected function getProductPageFromSitemap(array $sitemap)
    {
        foreach ($sitemap['sitemap'] as $page) {
            $url = $page['loc'];
            // Target's XML names follow the same rule as their page URLs convienently
            if ($this->isProductPage($url)) {
                $client = new Client();
                $xmlString = $client->request('GET', $url, [
                    'headers' => 
                        ['User-Agent' => env('USER_AGENT')]
                    ]
                )->getBody();
                @$xml = simplexml_load_string($xmlString);
                if ($xml instanceof SimpleXMLElement) {
                    foreach ($xml->url as $page) {
                        yield (string) $page->loc;
                    }
                }
            }
        } 
    }

    /**
     * Returns the product page sitemap from the sitemap array
     * @param  array  $sitemaps The array of 
     * @return string           The URL for the product page sitemap
     */
    protected function getProductPageMap(array $sitemaps)
    {
        return $sitemaps[0];
    }
}