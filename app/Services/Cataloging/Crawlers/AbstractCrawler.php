<?php
namespace App\Services\Cataloging\Crawlers;

use SimpleXMLElement;
use App\Services\Cataloging\Scrapers\AbstractScraper;
use App\Services\Cataloging\APIs\CatalogInterface;
use App\Events\ProductUrlAddedEvent;
use Illuminate\Support\Facades\Log;
use Robots\RobotsTxt;
use Robots\Exceptions\MissingRobotsTxtException;
use GuzzleHttp\Pool;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\TransferStats;

abstract class AbstractCrawler
{
    public static $useSitemap = false;

    // REMOVE ME IN PRODUCTION
    protected $testProductCount = 0;
    
    /**
     * The URLs that the crawler needs to visit
     * @var array
     */
    protected $needToVisit;

    /**
     * The URLs that the crawler has visited
     * @var array
     */
    protected $visited;

    /**
     * An instance of the AbstractScraper implementation for a given crawler.
     * This is setup in the Service Container
     * @var AbstractScraper
     */
    protected $scraper;

    /**
     * An instance of an attached CatalogInterface implementation. May not exist for each Crawler implementation
     * @var CatalogInterface
     */
    protected $catalogApi;

    /**
     * Check if a URL is a product page to be submitted to the db
     * @param  string  $url [description]
     * @return boolean      [description]
     */
    abstract protected function isProductPage($url);

    /**
     * Check if a discovered URL is relevant to the site being crawled
     * @param  string  $url
     * @return boolean
     */
    abstract protected function isRelevantUrl($url);

    /**
     * Retrieve the base (e.g. starting) URL for the store to crawl
     * @return string
     */
    abstract public function getBaseUrl();

    /**
     * A Generator to iterate over for product page URLs
     * @param  array  $sitemap The sitemap (converted to an array) that contains the URLs to the XML sitemap files for product pages.
     *                         Some website sitemaps have multiple levels, this needs to be the one where the URLs go directly to the product page listings
     * @return string          This method needs to yield a URL string
     */
    abstract protected function getProductPageFromSitemap(array $sitemap);

    /**
     * Returns the product page sitemap that is to be used in this::getProductPageFromSitemap()
     * 
     * @param  array  $sitemaps An array of the provided sitemaps
     * @return array
     */
    abstract protected function getProductPageMap(array $sitemaps);

    /**
     * Inject an instance of a AbstractScraper implementation to scrape the URLs this crawler adds, as well as the Model to store
     * the URLs on
     * This will be injected from the Service Provider
     * @param AbstractScraper $scraper A scraper implementation instance
     */
    public function __construct(AbstractScraper $scraper)
    {
        $this->scraper = $scraper;
        $this->visited = [];
    }

    /**
     * Remove querystring data
     * @param  string $url
     * @return string $url
     */
    protected function removeQueryString($url)
    {
        $url = explode("?", $url, 2)[0];

        return $url;
    }

    /**
     * Retrieves an arary of sitemap URLs for the website, or an empty array if there are none
     * @return array 
     */
    protected function downloadSitemaps()
    {
        $sitemaps = (new RobotsTxt())->setUserAgent(env('USER_AGENT'))->getSitemaps($this->getBaseUrl());
        return $sitemaps;
    }

    /**
     * Parses the sitemap XML into an array of URLs where the page listings can be found
     * @param  string $siteMapUrl A sitemap URL from the robots.txt file
     * @return array              An array containing URLs that link to the URL data/page listings. 
     *                            Note: The array will be keyed according to the site's sitemap XML. 
     *                                  Each crawler implementation will need to parse the array for the URLs they need.
     */
    protected function loadSitemap($siteMapUrl)
    {
        $client    = new Client();
        $xmlString = $client->request('GET', $siteMapUrl, [
            'headers' => 
                ['User-Agent' => env('USER_AGENT')]
            ]
        )->getBody();
        // Try to load the XML
        @$xml = simplexml_load_string($xmlString);

        if ($xml instanceof SimpleXMLElement) {
            // Convert XML to array with JSON encoding then decoding
            $sitemap = json_decode(json_encode($xml), true);
        } else {
            $sitemap = false;
            Log::error("Invalid XML from $siteMapUrl");
        }

        return $sitemap;
    }

    /**
     * Dispatches ScrapeProductPage jobs for product pages found in the sitemap
     * 
     * @return void
     */
    protected function dispatchFromSitemap()
    {
        $sitemaps = $this->downloadSitemaps();
        if (!$sitemaps) {
            $base = $this->getBaseUrl();
            throw new Exception("No sitemap found for {$base}. Try setting the Crawler's useSitemap property to false");
        }
        // pick the one that has the product page maps and load it into an array
        $sitemap  = $this->loadSitemap($this->getProductPageMap($sitemaps));
        // Iterate over the generator to get product page URLs
        foreach ($this->getProductPageFromSitemap($sitemap) as $url) {
            // TESTING purposes
            if (\App::environment('local', 'testing')) {
                if ($this->testProductCount > env('TEST_PRODUCT_LIMIT')) {
                    return;
                }
            }
            // END TESTING
            dispatch((new \App\Jobs\ScrapeProductPage($this->scraper, $url))->onQueue('products'));
            Log::info("NEW PRODUCT URL (SITEMAP): " . $url);
            // TESTING
            if (\App::environment('local', 'testing')) {
                $this->testProductCount += 1;
            }
            // END TESTING
        }
    }

    /**
     * Attach a CatalogInterface API implementation for the Crawler to use
     * @param  CatalogInterface $catalogApi 
     * @return void
     */
    public function attachCatalogApi(CatalogInterface $catalogApi)
    {
        $this->catalogApi = $catalogApi;
    }

    /**
     * Remove the CatalogInterface API implementation from the crawler. Only being used for automated tests currently
     * @return void
     */
    public function removeCatalogApi()
    {
        $this->catalogApi = null;
    }

    /**
     * Returns a CatalogInterface API implementation if one is attached
     * @return CatalogInterface | false
     */
    public function getCatalogApi()
    {
        $catalogApi = false;
        if (!is_null($this->catalogApi)) {
            $catalogApi = $this->catalogApi;
        } 

        return $catalogApi;
    }

    /**
     * Crawl from the starting URL using Guzzle Pool
     * @param  string $startingUrl 
     * @return void
     */
    protected function webCrawl($startingUrl)
    {
        $this->needToVisit[] = $startingUrl;
        $currentUrl = null;
        $client     = new Client([
            // This will let us get the request URL ($currentUrl) after execution
            'on_stats' => function (TransferStats $stats) use (&$currentUrl) {
                $currentUrl = $stats->getEffectiveUri()->__toString();
            },
            'headers' => [
                'User-Agent' => env('USER_AGENT'),
            ]
        ]);

        $pool = new Pool($client, $this->toVisit(), [
            // TODO: Need a way to make this work with concurrent requests. Problem is there's only 1 URL in $needTovisit array at start
            'concurrency' => 1,
            'fulfilled'   => function ($response, $index) use (&$currentUrl) {
                $html = $response->getBody()->getContents();
                $this->extractLinks($html, $currentUrl);
                // Add the URL to the visited set
                $this->visited[] = $currentUrl;
                Log::info(count($this->visited) . " URLS VISITED ************* $currentUrl");
            },
            'rejected' => function ($reason, $index) {
                Log::info("Request failed: " . $reason);
            },
        ]);
        $promise = $pool->promise();
        $promise->wait();
    }

    /**
     * Handles the site crawl
     * @todo   Rename to crawl()
     * @param  string $startingUrl
     * @return void
     */
    public function crawl($startingUrl)
    {
        // Check if there's an API implementation
        if ($this->getCatalogApi()) {
            $this->getCatalogApi()->updateCatalog();
        // Check if sitemap implementation
        } elseif ($this::$useSitemap == true) {
            $this->dispatchFromSitemap();
        // Crawlin in the dark searching for the answers
        } else {
            $this->webCrawl($startingUrl);
        }
    }

    /**
     * Generator for the crawl method to provide links
     * @return Request A Guzzle/Psr7/Request instance
     */
    protected function toVisit() 
    {
        while (!empty($this->needToVisit)) {
            // Testing 
            if ($this->testProductCount == env('TEST_PRODUCT_LIMIT')) {
                return;
            }
            // End testing
            $url = array_shift($this->needToVisit);
            yield new Request('GET', $url);
        }
    }

    /**
     * Get all the subpages at a domain
     * @todo  There's probably a way to make this faster
     * @param string $html The HTML markup of a page
     * @param string $url  The URL of the page the crawler is on
     * @return void
     */
    protected function extractLinks($html, $url)
    {       
        // This will process robots.txt rules for us
        $robotsRules = (new RobotsTxt())->setUserAgent(env('USER_AGENT'));
        $dom         = new \DOMDocument('1.0');
        @$dom->loadHTML($html);
        $anchors = $dom->getElementsByTagName('a');
        foreach ($anchors as $element) {
            // TESTING - No more new products
            if ($this->testProductCount == env('TEST_PRODUCT_LIMIT')) {
                return;
            }
            // END TESTING
            $href = $element->getAttribute('href');
            $href = $this->makeCrawlable($href, $url);
            // crawler specific filtering - I'd like to move these checks out of the method if possible
            $relevant = $this->isRelevantUrl($href);
            if (!$relevant) {
                continue;
            }
            // check if URL meets our robots.txt rules
            try {
                $allowed = $robotsRules->isAllowed($href);
            } catch (MissingRobotsTxtException $e) {
                $allowed = true;
            }

            if(!$allowed) {
                Log::info("UNAUTHORIZED TO CRAWL URL: {$href}");
                continue;
            }
            // Make sure this is a new URL
            if (!in_array($href, $this->needToVisit) && !in_array($href, $this->visited) && $href != $url) {
                $this->needToVisit[] = $href;
                if ($this->isProductPage($href)) { 
                    Log::info("NEW PRODUCT URL: " . $href);
                    // Dispatch scraper job
                    dispatch((new \App\Jobs\ScrapeProductPage($this->scraper, $href))->onQueue('products'));
                    // TESTING
                    if (\App::environment('local', 'testing')) {
                        $this->testProductCount += 1;
                    }
                    // END TESTING
                }
            } 
        }
    }

    /**
     * Makes a crawlable URL given a href value
     * @param  string $href The path or URL from a href tag
     * @return string $url  An absolute URL with without an anchor tag or trailing slash
     */
    private function makeCrawlable($href, $currentPageUrl)
    {
        // If relative URL, build an absolute url
        if (0 !== strpos($href, 'http')) {
            $href = $this->makeAbsoluteUrl($href, $currentPageUrl);
        }
        // @todo decide if this is needed - do we want to crawl over the same path with different query args?
        $href = $this->removeQueryString($href);
        // Remove anchor tags from link
        $href = strpos($href, '#') !== false ? strstr($href, '#', true) : $href;
        // Remove trailing slashes
        $href = rtrim($href, '/');

        return $href;
    }

    /**
     * Makes an absolute URL from a relative href
     * 
     * @param  string $href           
     * @param  string $currentPageUrl The URL of the page the link is on
     * @return string
     */
    private function makeAbsoluteUrl($href, $currentPageUrl)
    {
        $path = '/' . ltrim($href, '/');
        $parts = parse_url($currentPageUrl); // $currentPageUrl will be an absolute URL
        $href = $parts['scheme'] . '://';
        if (isset($parts['user']) && isset($parts['pass'])) {
            $href .= $parts['user'] . ':' . $parts['pass'] . '@';
        }
        $href .= $parts['host'];
        if (isset($parts['port'])) {
            $href .= ':' . $parts['port'];
        }
        $href .= $path;

        return $href;
    }
}