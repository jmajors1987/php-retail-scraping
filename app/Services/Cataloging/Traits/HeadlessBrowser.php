<?php
namespace App\Services\Cataloging\Traits;

use JonnyW\PhantomJs\Client as PhantomClient;
use Illuminate\Support\Facades\Log;

trait HeadlessBrowser
{
    /**
     * Load the HTML from the url post-js execution
     * @todo   Fix getEngine() call
     * @link   http://jonnnnyw.github.io/php-phantomjs/
     * @param  string $url
     * @return string $content The HTML as a string
     */
    public function getGeneratedHTML($url)
    {
        $content = '';
        $client  = PhantomClient::getInstance();
        $client->getEngine()->setPath('/usr/local/bin/phantomjs'); // Need to make this not neccesary

        $request  = $client->getMessageFactory()->createRequest($url, 'GET');
        $response = $client->getMessageFactory()->createResponse();

        $request->setViewportSize(1863, 990);
        $request->addHeader('User-Agent', env('USER_AGENT'));

        // Send the request
        $client->send($request, $response);
        if($response->getStatus() === 200) {
            // Dump the requested page content
            $content = $response->getContent();
        // tends to just load forever...?
        } elseif ($response->getStatus() === 408) {
            // Content still there sometimes? Still need to investigate this
            $content = $response->getContent();
            Log::info("TIMEOUT -- $url");
        }

        return $content;
    }

    /**
     * Overrides AbstractCrawler::webCrawl method so to use getGeneratedHTML() as the links to product pages are generated via JS
     * @todo  This could be problematic as there's no reason someone couldn't use this trait OUTSIDE of a Crawler implementation. Should throw an Exception
     * @param  string $startingUrl
     * @return void
     */
    public function webCrawl($startingUrl)
    {
        $this->needToVisit[] = $startingUrl;
        while (!empty($this->needToVisit)) {
            // TESTING - End script if we've hit the max products we want
            if ($this->testProductCount >= env('TEST_PRODUCT_LIMIT')) {
                return;
            }
            // END TESTING
            $url  = array_shift($this->needToVisit);
            $html = $this->getGeneratedHTML($url);
            $this->extractLinks($html, $url);
            // Add the URL to the visited set
            $this->visited[] = $url;
            Log::info(count($this->visited) . " URLS VISITED *************");
        }
    }
}