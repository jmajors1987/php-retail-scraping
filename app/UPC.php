<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Represents a Universl Product Code
 */
class UPC extends Model
{
    protected $table = 'upcs';

    /**
     * Make all columns mass assignable
     * @var array
     */
    protected $guarded = [];

    /**
     * Get the actual Product data for a UPC
     */
    public function products()
    {
        return $this->hasMany('App\Product', 'upc_id');
    }

    /**
     * Get the Marketplace listings for a UPC
     */
    public function listings()
    {
        return $this->hasMany('App\Listing', 'upc_id');
    }

    /**
     * Get the price Differentials for a UPC
     */
    public function differentials()
    {
        return $this->hasMany('App\Differential', 'upc_id');
    }
}
