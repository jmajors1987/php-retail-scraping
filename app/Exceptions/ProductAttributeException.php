<?php

namespace App\Exceptions;

use Exception;
use App\Product;

class ProductAttributeException extends Exception
{

    public function __construct(Product $product, $code=0)
    {
        $message = $this->makeErrorMessage($product);
        parent::__construct($message, $code);
    }

    /**
     * Prepares the error message
     * @param  Product $product
     * @return string           
     */
    public static function makeErrorMessage(Product $product)
    {
        $errors  = join(', ', $product->getErrors());
        $message = "Unable to save the data for '$product->title' from $product->url. Errors: $errors";

        return $message;
    }
}