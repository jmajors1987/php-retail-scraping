<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    /**
     * Array of validation errors
     * @var array
     */
    protected $errors;

    /**
     * Make all columns mass assignable
     * @var array
     */
    protected $guarded = [];

    /**
     * Get the UPC this listing belongs to
     */
    public function upc()
    {
        return $this->belongsTo('App\UPC');
    }

    public function differentials()
    {
        return $this->hasMany('App\Differential');
    }

    /**
     * Returns the errors when a Product::save() call was unsuccessful
     * @return array
     */
    public function getErrors()
    {
        return $this->errors;
    }

    /**
     * Return whether or not there were errors on saving this Product to the database
     * @return boolean
     */
    public function hasErrors()
    {
        return (count($this->errors) > 0);
    }

     /**
     * Validate that the scraped product fields are usable
     * @return bool             
     */
    public function isValid()
    {
        $valid    = false;
        $attributes = $this->getAttributes();
        // TODO: May want to reconsider this validation design if too many fields become added
        array_walk($attributes, function(&$value, $key) {
            switch ($key) {
/*                case 'description':
                    if ($value === false) {
                        $this->errors[] = "No description found";
                    } elseif (strlen($value) > 5000) {
                        $this->errors[] = "Description too long";
                    }
                    break;*/
                case 'price':
                    if ($value === false) {
                        $this->errors[] = "No price found";  
                    } elseif (!is_numeric($value)) {
                        $this->errors[] = "Invalid price: $value";
                    }
                    break;
                case 'upc':
                    if ($value === false) {
                        $this->errors[] = "No UPC available";
                    }
                    break;
                case 'title':
                    if ($value === false) {
                        $this->errors[] = "Title not found";
                    }
                    break;
                case 'stock':
                    if ($value == 'unavailable') {
                        $this->errors[] = "Stock unavailable";
                    }
                    break;
                case 'available':
                    if ($value === false) {
                        $this->errors[] = "Not available online";
                    }
                    break;
                case 'marketplace':
                    if ($value === true) {
                        $this->errors[] = "Only available via marketplace";
                    }
                    break;
                default:
                    break;
            }
        });

        if (count($this->errors) == 0) {
            $valid = true;
        }

        return $valid;
    }
}
