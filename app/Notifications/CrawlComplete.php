<?php

namespace App\Notifications;

use App\Services\Cataloging\Crawlers\AbstractCrawler;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class CrawlComplete extends Notification implements ShouldQueue
{
    use Queueable;

    /**
     * Crawler implementation
     * @var AbstractCrawler
     */
    protected $crawler;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(AbstractCrawler $crawler)
    {
        $this->crawler = $crawler;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $url = $this->crawler->getBaseUrl();
        return (new MailMessage)
                    ->subject("Completed Crawl: $url")
                    ->greeting('Crawl Complete!')
                    ->line("Successfully crawled $url. Product URLs have been added to the queue for scraping.")
                    ->line("Data, data ,data");
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
