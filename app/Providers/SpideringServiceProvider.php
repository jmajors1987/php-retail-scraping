<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Queue;
use App\Services\Cataloging\Scrapers\WalmartScraper;
use App\Services\Cataloging\Scrapers\AbstractScraper;
use App\Services\Cataloging\Crawlers\WalmartCrawler;
use App\Services\Cataloging\Crawlers\TargetCrawler;
use App\Services\Cataloging\Scrapers\TargetScraper;
use App\Services\Cataloging\APIs\WalmartCatalogApi;
use App\Services\Listings\Amazon\AmazonListings;

class SpideringServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = true;

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('AmazonListings', function($app) {
            return new AmazonListings();
        });

        // Inject an specific Scraper implementations into a Crawler
        $this->registerScraperForCrawler(WalmartCrawler::class, WalmartScraper::class);
        $this->registerScraperForCrawler(TargetCrawler::class, TargetScraper::class);
        // Attach an API implementation to the WalmartCrawler
        $this->registerCatalogApi(WalmartCrawler::class, WalmartCatalogApi::class);
    }

    protected function registerScraperForCrawler($crawlerClassName, $scraperClassName)
    {
        $this->app->when($crawlerClassName)
            ->needs(AbstractScraper::class)
            ->give(function () use ($scraperClassName) {
                return $this->app->make($scraperClassName);
        });
    }

    protected function registerCatalogApi($crawlerClassName, $catalogApi)
    {
        $this->app->resolving($crawlerClassName, function ($crawler, $app) use ($catalogApi) {
            $crawler->attachCatalogApi($this->app->make($catalogApi));
        });
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [
            WalmartCrawler::class,
            TargetCrawler::class,
            AmazonListings::class
        ];
    }
}
