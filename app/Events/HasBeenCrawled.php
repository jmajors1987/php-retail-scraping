<?php

namespace App\Events;

use App\Services\Cataloging\Crawlers\AbstractCrawler;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class HasBeenCrawled
{
    use InteractsWithSockets, SerializesModels;

    /**
     * The Crawler implementation
     * @var AbstractCrawler
     */
    protected $crawler;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(AbstractCrawler $crawler)
    {
        $this->crawler = $crawler;
    }

    /**
     * Returns the crawled URL
     * @return string URL
     */
    public function getCrawler()
    {
        return $this->crawler;
    }
}
