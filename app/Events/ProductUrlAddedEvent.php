<?php

namespace App\Events;

use App\Services\Cataloging\Scrapers\AbstractScraper;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class ProductUrlAddedEvent
{
    use InteractsWithSockets, SerializesModels;

    /**
     * An instance of a AbstractScraper implementation
     * @var AbstractScraper
     */
    protected $scraper;

    /**
     * The URL added to scrape
     * @var string
     */
    protected $url;

    /**
     * Create a new event instance.
     * 
     * @return void
     */
    public function __construct(AbstractScraper $scraper, $url)
    {
        $this->scraper = $scraper;
        $this->url     = $url;
    }

    public function getScraper()
    {
        return $this->scraper;
    }

    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
