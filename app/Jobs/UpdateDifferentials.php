<?php

namespace App\Jobs;

use App\UPC;
use App\Differential;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Log;

class UpdateDifferentials implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        // When processing large amounts of data, the cursor method may be used to greatly reduce your memory usage
        foreach (UPC::with('products')->cursor() as $upc) {
            // For each UPC, get the products (should be 1 per store)
            foreach ($upc->products as $product) {
                //  for each product, get the listings (should just be 1, but my current amazon implementation is weird so maybe not)
                foreach ($product->upc->listings as $listing) {
                    $differential = new Differential; // for testing
                    // Check if worth
                    $relevance = $differential->isRelevant($product, $listing);
                    // if relevance ...
                    if ($relevance) {
                        $differential->difference = $differential->calculatePriceDifferential($product, $listing);
                        $differential->upc_id     = $upc->id;
                        $differential->product_id = $product->id;
                        $differential->listing_id = $listing->id;
                        $differential->save();
                    }
                }
            }     
        }
    }
}
