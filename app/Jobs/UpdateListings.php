<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Services\Listings\AbstractListings;

class UpdateListings implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;

    protected $marketplaceListings;

    /**
     * Create a new job instance.
     * 
     * @return void
     */
    public function __construct(AbstractListings $marketplaceListings)
    {
        $this->marketplaceListings = $marketplaceListings;
    }

    /**
     * Execute the job.
     * @todo  add store arg
     * @return void
     */
    public function handle()
    {
        $this->marketplaceListings->checkProducts();
    }

    /**
     * Job failed, notify admin
     *         
     * @param  Exception $exception [description]
     * @return [type]               [description]
     */
    public function failed(Exception $exception)
    {
        Log::info("** JOB FAILED **  Update Listings Failed: " . $exception->getMessage());
    }
}
