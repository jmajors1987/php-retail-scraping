<?php

namespace App\Jobs;

use Exception;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Services\Cataloging\Scrapers\AbstractScraper;
use Illuminate\Support\Facades\Log;

class ScrapeProductPage implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;

    /**
     * The scraper implementation for a website
     * @var AbstractScraper
     */
    protected $scraper;

    /**
     * The URL to scrape
     * @var string
     */
    protected $url;
    
    /**
     * Create a new job instance.
     * @return void
     */
    public function __construct(AbstractScraper $scraper, $url)
    {
        $this->scraper = $scraper;
        $this->url     = $url;
    }

    /**
     * Getter for the URL property. Added for testability
     * @return string The URL to be scraped
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Getter for the scraper property. Added for testability
     * @return AbstractScraper
     */
    public function getScraper()
    {
        return $this->scraper;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->scraper->updateProductDetails($this->url);
    }

    /**
     * Job failed, notify admin
     *         
     * @param  Exception $exception The exception that caused the job to fail
     * @return void
     */
    public function failed(Exception $exception)
    {
        Log::info("** JOB FAILED **  Scraping job failed for $this->url " . $exception->getMessage());
    }
}
