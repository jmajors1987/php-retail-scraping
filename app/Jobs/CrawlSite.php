<?php

namespace App\Jobs;

use Exception;
use App\Services\Cataloging\Crawlers\AbstractCrawler;
use App\Events\HasBeenCrawled;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class CrawlSite implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;

    protected $crawler;

    protected $url;

    /**
     * Create a new job instance.
     * @param AbstractCrawler $crawler An instance of a crawler extending AbstractCrawler
     * @return void
     */
    public function __construct(AbstractCrawler $crawler)
    {
        $this->crawler = $crawler;
        $this->url     = $crawler->getBaseUrl();
    }

    /**
     * Getter for the crawler. Needed for testability
     * @return AbstractCrawler
     */
    public function getCrawler()
    {
        return $this->crawler;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->crawler->crawl($this->url);
        event(new HasBeenCrawled($this->crawler));
    }

    /**
     * Job failed, notify admin
     *         
     * @param  Exception $exception [description]
     * @return [type]               [description]
     */
    public function failed(Exception $exception)
    {
        // job failed.. send a notification
    }
}
