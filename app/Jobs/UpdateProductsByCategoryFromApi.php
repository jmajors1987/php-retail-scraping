<?php

namespace App\Jobs;
use App\Services\Cataloging\APIs\CreatesFromCategoryInterface;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class UpdateProductsByCategoryFromApi implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var CreatesFromCategoryInterface
     */
    protected $catalogApi;

    /**
     * @var string
     */
    protected $categoryId;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(CreatesFromCategoryInterface $catalogApi, $categoryId)
    {
        $this->catalogApi = $catalogApi;
        $this->categoryId = $categoryId;
    }

    /**
     * Getter for category ID. Added for testability
     * @return string
     */
    public function getCateogryId()
    {
        return $this->categoryId;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->catalogApi->updateProductData($this->categoryId);  
    }

    /**
     * Job failed, notify admin
     *         
     * @param  Exception $exception The exception that caused the job to fail
     * @return void
     */
    public function failed(Exception $exception)
    {
        Log::info("** JOB FAILED **  Scraping job failed for $this->url " . $exception->getMessage());
    }
}
