<?php

namespace App;

use App\UPC;
use App\Product;
use App\Listing;
use Illuminate\Database\Eloquent\Model;

class Differential extends Model
{   
    // Many to One UPC
    public function upc()
    {
        return $this->belongsTo('App\UPC');
    }

    // Many to One Product
    public function product()
    {
        return $this->belongsTo('App\Product');
    }

    // many to One Listing
    public function listing()
    {
        return $this->belongsTo('App\Listing');
    }

    public function calculatePriceDifferential(Product $product, Listing $listing)
    {
        $productPrice = $product->price;
        $listingPrice = $listing->lowest_price;

        return abs($productPrice - $listingPrice);
    }

    /**
     * Check whether a price differential is relevant
     * @todo  build this out
     * @return boolean
     */
    public function isRelevant(Product $product, Listing $listing)
    {
        return true;
    }
}

   