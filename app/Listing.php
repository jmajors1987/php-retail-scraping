<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Listing extends Model
{
    protected $table = 'marketplace_listings';

    /**
     * Make all columns mass assignable
     * @var array
     */
    protected $guarded = [];

    /**
     * Get the UPC this listing belongs to
     */
    public function upc()
    {
        return $this->belongsTo('App\UPC');
    }

    public function differentials()
    {
        return $this->hasMany('App\Differential');
    }
}
