<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App;
use ReflectionException;
use App\Jobs\CrawlSite;
use App\Jobs\CatalogWithApi;

class DispatchCrawler extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'products:update {store}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "Dispatches the store's product cataloging API implementation";

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     * @return void
     */
    public function handle()
    {
        $store = ucfirst($this->argument('store'));
        // Try to make the crawler from the store name
        try {
            // Must be registered in AppServiceProdiver
            $crawler = app("App\Services\Cataloging\Crawlers\\{$store}Crawler");
            // Go go
            dispatch(new CrawlSite($crawler));
            $this->info("Dispatching Crawler to the queue");
        } catch (ReflectionException $e) {
            $this->error("No Crawler found for $store");
        }
    }
}
