<?php

namespace App\Console\Commands;

use App\Jobs\UpdateDifferentials;
use Illuminate\Console\Command;

class DispatchUpdateDifferentials extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'differentials:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Updates the differential data on products vs. listings';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        dispatch(new UpdateDifferentials());
        $this->info("Calculating price differences between Products and Listings. Will only save relevant differentials.");
    }
}
