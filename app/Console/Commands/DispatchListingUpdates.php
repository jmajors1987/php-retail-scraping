<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App;
use ReflectionException;
use App\Jobs\UpdateListings;

class DispatchListingUpdates extends Command
{
    /**
     * The name and signature of the console command.
     * @var string
     */
    protected $signature = 'listings:update {marketplace}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "Dispatches a job to update the listings for a marketplace";

    /**
     * DispatchListingUpdates constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $marketplace = ucfirst($this->argument('marketplace'));
        try {
            // Registered in SpideringServiceProvider.php
            $marketplaceListings = app($marketplace . 'Listings');
            dispatch(new UpdateListings($marketplaceListings));
            $this->info("Dispatching listing updates for $marketplace to the queue");
        } catch (ReflectionException $e) {
            $this->error("No marketplace listings update implementation found for $marketplace");
        }
    }
}
