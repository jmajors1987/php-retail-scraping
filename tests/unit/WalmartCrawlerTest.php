<?php


class WalmartCrawlerTest extends TestCase
{
    public function testGetBaseUrl()
    {
        $walmartCrawler = app("App\Services\Cataloging\Crawlers\WalmartCrawler");
        $this->assertEquals('https://www.walmart.com', $walmartCrawler->getBaseurl());
    }
}