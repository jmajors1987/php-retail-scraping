<?php


class TargetCrawlerTest extends TestCase
{
    public function testGetBaseUrl()
    {
        $targetCrawler = app("App\Services\Cataloging\Crawlers\TargetCrawler");
        $this->assertEquals('http://www.target.com', $targetCrawler->getBaseurl());
    }
}