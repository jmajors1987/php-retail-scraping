<?php

use App\Product;
use App\Jobs\CrawlSite;
use App\Services\Cataloging\Crawlers\TargetCrawler;
use App\Services\Cataloging\Crawlers\WalmartCrawler;
use Illuminate\Support\Facades\Queue;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class SpideringCommandsTest extends TestCase
{
    use DatabaseMigrations;

    public function testProductsUpdateTargetCommand()
    {
        Queue::fake();
        Artisan::call('products:update', ['store' => 'target']);
        Queue::assertPushed(CrawlSite::class, function($job) {
            return get_class($job->getCrawler()) == TargetCrawler::class;
        });
    }

    public function testProductsUpdateWalmartCommand()
    {
        Queue::fake();
        Artisan::call('products:update', ['store' => 'walmart']);
        Queue::assertPushed(CrawlSite::class, function($job) {
            return get_class($job->getCrawler()) == WalmartCrawler::class;
        });
    }
}