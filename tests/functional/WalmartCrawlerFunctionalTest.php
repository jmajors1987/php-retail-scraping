<?php

use Illuminate\Support\Facades\Queue;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\Jobs\UpdateProductsByCategoryFromApi;
use App\Jobs\ScrapeProductPage;
use App\Services\Cataloging\Crawlers\WalmartCrawler;

class WalmartCrawlerFuntionalTest extends TestCase
{
    use DatabaseMigrations;

    public function testCrawlWithApiImplementation()
    {
        Queue::fake();

        $walmartCrawler = resolve("App\Services\Cataloging\Crawlers\WalmartCrawler");
        $walmartCrawler->crawl('https://www.walmart.com');   
        // Check that our update category job was added to the queue
        Queue::assertPushed(UpdateProductsByCategoryFromApi::class, function ($job) {
            // Category ID may change if Walmart changes their open API implementation. Check that first on failure @ https://developer.walmartlabs.com/io-docs
            return $job->getCateogryId() == '1334134_5899871_8844202';
        });     
    }

    /**
     * Tests crawling the site without the walmart API to check if the correct product URL is added to be scraped
     */
    public function testCrawlWithoutApiImplementation()
    {
        Queue::fake();
        $walmartCrawler = $this->app->make("App\Services\Cataloging\Crawlers\WalmartCrawler");
        $walmartCrawler->removeCatalogApi();
        $walmartCrawler->crawl('https://www.walmart.com/browse/5428');   
        // Check that our update category job was added to the queue
        Queue::assertPushed(ScrapeProductPage::class, function ($job) {
            // Category ID may change if Walmart changes their open API implementation. Check that first on failure
            // TODO: This is currently not very intuitive. Check laravel.log for the page where it found the product.
            // This is because it's not using the headless browser implementation
            return $job->getUrl() == 'https://www.walmart.com/ip/Zero-Gravity-Chairs-Case-Of-2-Tan-Lounge-Patio-Chairs-Outdoor-Yard-Beach-New/44924709';
        });     
    }
}