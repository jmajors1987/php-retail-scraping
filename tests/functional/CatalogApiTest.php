<?php

use App\Product;
use Illuminate\Support\Facades\Queue;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\Jobs\UpdateProductsByCategoryFromApi;
use App\Services\Cataloging\APIs\WalmartCatalogApi;

class CatalogApiTest extends TestCase
{
    use DatabaseMigrations;

    /**
     * @dataProvider walmartApiProductDataProvider
     */
    public function testWalmartApiProductData($categoryId, $expected)
    {
        $api = new WalmartCatalogApi;
        $api->updateProductData($categoryId);
        $product = Product::first()->toArray();
        if ($product) {
            $this->assertEquals($product['store'], $expected['store']);
            $this->assertEquals($product['title'], $expected['title']); // TODO: Remove all whitespace
            $this->assertEquals($product['price'], $expected['price']);
            $this->assertEquals(
                // Remove all whitespace
                trim(preg_replace("/(\t|\n|\v|\f|\r| |\xC2\x85|\xc2\xa0|\xe1\xa0\x8e|\xe2\x80[\x80-\x8D]|\xe2\x80\xa8|\xe2\x80\xa9|\xe2\x80\xaF|\xe2\x81\x9f|\xe2\x81\xa0|\xe3\x80\x80|\xef\xbb\xbf)+/", '', $product['description'])), 
                trim(preg_replace('/\s+/', '', $expected['description'])));
            $this->assertEquals($product['on_sale'], $expected['on_sale']);
            $this->assertEquals($product['category'], $expected['category']);
            $this->assertEquals($product['num_reviews'], $expected['num_reviews']);
            $this->assertEquals($product['images'], $expected['images']);
        } else {
            throw new \Exception("Unable to find the product scraped from $url in testTargetScraperProductData method");
        }
    }

    public function walmartApiProductDataProvider()
    {
        return [
            ['5428_1218910_1101444', 
                [
                    'store'       => 'walmart',
                    'title'       => 'Safavieh Courtyard Yvette Power-Loomed Indoor/Outdoor Area Rug or Runner',
                    'price'       => 93.59,
                    'description' => 
                        "&lt;p&gt;&lt;b&gt;&lt;br&gt;Safavieh Courtyard Clapton Outdoor Rug:&lt;/b&gt;&lt;/p&gt;&lt;ul&gt;&lt;li&gt;Sisal weave&lt;/li&gt;&lt;li&gt;Made of polypropylene&lt;/li&gt;&lt;li&gt;Indoor/outdoor polypropylene rug is weather-resistant&lt;/li&gt;&lt;li&gt;Cleans easily with a garden hose&lt;/li&gt;&lt;li&gt;Imported; made in Turkey&lt;/li&gt;&lt;li&gt;Available sizes: (63&quot; x 91&quot;), (2' x 3' 7&quot;), (4' x 5' 7&quot;), (6' 7&quot; x 9' 6&quot;), (8' x 11' 2&quot;)&lt;/li&gt;&lt;li&gt;Modern style to elevate your decor&lt;/li&gt;&lt;/ul&gt;",
                    'on_sale'     => 0,
                    'num_reviews' => 0, // field not provided for this item
                    'category'    => 'Home/Decor/Rugs/Indoor/Outdoor Rugs',
                    'images'      => '{"largeImage":"https:\/\/i5.walmartimages.com\/asr\/f41d0fb0-2587-477d-985e-bb956eb9eb0b_1.934aa613467c167e84ce88f924ef3dcb.jpeg?odnHeight=450&odnWidth=450&odnBg=ffffff","mediumImage":"https:\/\/i5.walmartimages.com\/asr\/f41d0fb0-2587-477d-985e-bb956eb9eb0b_1.934aa613467c167e84ce88f924ef3dcb.jpeg?odnHeight=180&odnWidth=180&odnBg=ffffff","smallImage":"https:\/\/i5.walmartimages.com\/asr\/f41d0fb0-2587-477d-985e-bb956eb9eb0b_1.934aa613467c167e84ce88f924ef3dcb.jpeg?odnHeight=100&odnWidth=100&odnBg=ffffff"}',
                ]
            ]
        ];
    }
}