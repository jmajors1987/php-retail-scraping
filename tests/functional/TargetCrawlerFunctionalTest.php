<?php

use App\Jobs\ScrapeProductPage;
use App\Services\Cataloging\Scrapers\TargetScraper;
use Illuminate\Support\Facades\Queue;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class TargetCrawlerFuntionalTest extends TestCase
{
    use DatabaseMigrations;

    /**
     * Tests using the sitemap to get the first product URL and add a job to the queue for that URL to be scraped
     */
    public function testCrawlWithSiteMap()
    {
        // Don't actually add the job to the queue
        Queue::fake();

        $targetCrawler = app("App\Services\Cataloging\Crawlers\TargetCrawler");
        $targetCrawler->crawl('http://www.target.com');
        // Check that our scraping job was added to the queue with the correct URL
        Queue::assertPushed(ScrapeProductPage::class, function ($job) {
            // URL may need to change if Target updates their sitemap. Check this on test failure
            return $job->getUrl() == 'http://www.target.com/p/melissa-doug-174-construction-worker-puppet-with-detachable-wooden-rod-for-animated-gestures/-/A-10028672';
        });
        // Assert that the scraper provided is the TargetScraper class
        Queue::assertPushed(ScrapeProductPage::class, function ($job) {
            return get_class($job->getScraper()) == TargetScraper::class;
        });
        // Assert the job was pushed to the 'products' queue
        Queue::assertPushedOn('products', ScrapeProductPage::class);
    }

    /**
     * Tests crawling the site without the sitemap to get the first product URL and add a job to the queue for that URL to be scraped
     */
    public function testCrawlWithoutSiteMap()
    {
        Queue::fake();

        $targetCrawler = app("App\Services\Cataloging\Crawlers\TargetCrawler");
        $targetCrawler::$useSitemap = false;
        $targetCrawler->crawl('http://www.target.com');       
        // Check that our scraping job was added to the queue with the correct URL
        Queue::assertPushed(ScrapeProductPage::class, function ($job) {
            // URL may need to change if Target updates their sitemap. Check this on test failure
            return $job->getUrl() == 'http://www.target.com/p/women-s-racerback-tank-top-mossimo-supply-co-153/-/A-51727523';
        });
        // Assert that the scraper provided is the TargetScraper class
        Queue::assertPushed(ScrapeProductPage::class, function ($job) {
            return get_class($job->getScraper()) == TargetScraper::class;
        });
        // Assert the job was pushed to the 'products' queue
        Queue::assertPushedOn('products', ScrapeProductPage::class);
    }
}