<?php

use App\Product;
use App\Jobs\ScrapeProductPage;
use App\Services\Cataloging\Scrapers\TargetScraper;
use App\Services\Cataloging\Scrapers\WalmartScraper;
use Illuminate\Support\Facades\Queue;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ScrapeProductPageTest extends TestCase
{
    use DatabaseMigrations;

    /**
     * @dataProvider addsTargetProductProvider
     */
    public function testTargetScraperAddsProduct($url, $expected)
    {
        $job = new ScrapeProductPage(new TargetScraper, $url);
        $job->handle();
        $this->assertEquals($expected, Product::count());
    }

    /**
     * @dataProvider addsWalmartProductProvider
     */
    public function testWalmartScraperAddsProduct($url, $expected)
    {
        $job = new ScrapeProductPage(new WalmartScraper, $url);
        $job->handle();
        $this->assertEquals($expected, Product::count());
    }

    /**
     * @depends      testTargetScraperAddsProduct
     * @dataProvider targetProductDataProvider
     */
    public function testTargetScraperProductData($url, $expected)
    {
        $job = new ScrapeProductPage(new TargetScraper, $url);
        $job->handle();
        $product = Product::where('url', $url)->first()->toArray();
        if ($product) {
            $this->assertEquals($product['store'], $expected['store']);
            $this->assertEquals($product['title'], $expected['title']); // TODO: Remove all whitespace
            $this->assertEquals($product['price'], $expected['price']);
            $this->assertEquals(
                // Remove all whitespace
                trim(preg_replace("/(\t|\n|\v|\f|\r| |\xC2\x85|\xc2\xa0|\xe1\xa0\x8e|\xe2\x80[\x80-\x8D]|\xe2\x80\xa8|\xe2\x80\xa9|\xe2\x80\xaF|\xe2\x81\x9f|\xe2\x81\xa0|\xe3\x80\x80|\xef\xbb\xbf)+/", '', $product['description'])), 
                trim(preg_replace('/\s+/', '', $expected['description'])));
            $this->assertEquals($product['on_sale'], $expected['on_sale']);
            $this->assertEquals($product['category'], $expected['category']);
            $this->assertEquals($product['num_reviews'], $expected['num_reviews']);
            $this->assertEquals($product['images'], $expected['images']);
            $this->assertEquals($product['url'], $url);

        } else {
            throw new \Exception("Unable to find the product scraped from $url in testTargetScraperProductData method");
        }
    }

    /**
     * @depends      testWalmartScraperAddsProduct
     * @dataProvider walmartProductDataProvider
     */
    public function testWalmartScraperProductData($url, $expected)
    {
        $job = new ScrapeProductPage(new WalmartScraper, $url);
        $job->handle();
        $product = Product::where('url', $url)->first()->toArray();
        if ($product) {
            $this->assertEquals($product['store'], $expected['store']);
            $this->assertEquals($product['title'], $expected['title']); // TODO: Remove all whitespace
            $this->assertEquals($product['price'], $expected['price']);
            $this->assertEquals(
                // Remove all whitespace
                trim(preg_replace("/(\t|\n|\v|\f|\r| |\xC2\x85|\xc2\xa0|\xe1\xa0\x8e|\xe2\x80[\x80-\x8D]|\xe2\x80\xa8|\xe2\x80\xa9|\xe2\x80\xaF|\xe2\x81\x9f|\xe2\x81\xa0|\xe3\x80\x80|\xef\xbb\xbf)+/", '', $product['description'])), 
                trim(preg_replace('/\s+/', '', $expected['description'])));
            $this->assertEquals($product['on_sale'], $expected['on_sale']);
            $this->assertEquals($product['category'], $expected['category']);
            $this->assertEquals($product['num_reviews'], $expected['num_reviews']);
            $this->assertEquals($product['images'], $expected['images']);
            $this->assertEquals($product['url'], $url);

        } else {
            throw new \Exception("Unable to find the product scraped from $url in testTargetScraperProductData method");
        }
    }

    public function addsTargetProductProvider()
    {
        return [
            // Check that the product is NOT added when price less than TargetScraper::FREE_SHIPPING_PRICE
            ['http://www.target.com/p/boys-pull-on-playwear-shorts-cat-jack-153/-/A-51595295?lnk=rec|adapthp1|top_sellers_location|adapthp1|51595295|2', 0],
            // Check that the product is NOT added when 'temporarily out of stock'
            ['http://www.target.com/p/spalding-nba-eco-composite-32-portable-basketball-hoop/-/A-10823468', 0],
            // Product that should work
            ['http://www.target.com/p/lego-174-batman-movie-the-batmobile-70905/-/A-51297761', 1]
        ];
    }

    public function addsWalmartProductProvider()
    {
        return [
            // Check that the product is NOT added when price less than WalmartScraper::FREE_SHIPPING_PRICE
            ['https://www.walmart.com/ip/Winnie-the-Pooh-Newborn-Baby-Girl-Ruffle-Detail-Sleep-n-Play/55434458', 0],
            // Product that should work
            ['https://www.walmart.com/ip/Bissell-PowerForce-Helix-Turbo-Bagless-Vacuum-1701/49369570', 1]
        ];
    }

    public function targetProductDataProvider()
    {
        return [
            ['http://www.target.com/p/huggies-174-little-movers-diapers-economy-plus-pack-select-size/-/A-15259526?lnk=rec|adaptpdph2|recently_viewed|adaptpdph2|15259526|3', 
                [
                    'store'       => 'target',
                    'title'       => 'Huggies® Little Movers Diapers Economy Plus Pack (Select Size)',
                    'price'       => 38.99,
                    'description' => 
                        "HUGGIES Little Movers Size 3 Diapers feature our New Moving Baby System*. Shaped for outstanding fit, Little Movers Diapers include a DryTouch Liner that absorbs on contact and Double Grip Strips for a comfy fit that lasts. Also, with a SnugFit* waistband and Leak Lock for up to 12 hours of leakage protection, Little Movers gives your moving baby the driest feeling, best fitting diaper ever. Even the most energetic crawlers, jumpers, and walkers are no match for Little Movers disposable diapers! Available in sizes 3 (16-28 lb.), 4 (22-37 lb.), 5 (27+ lb.), and 6 (35+ lb.).",
                    'on_sale'     => 0,
                    'num_reviews' => 10397,
                    'category'    => null,
                    'images'      => 'http://target.scene7.com/is/image/Target/15194807?wid=520&hei=520&fmt=pjpeg',
                ]
            ]
        ];
    }

    public function walmartProductDataProvider()
    {
        return [
            ['https://www.walmart.com/ip/Lifetime-48-V-Frame-Shatterproof-Portable-One-Hand-Height-Adjustable-Basketball-System-90585/50104319', 
                [
                    'store'       => 'walmart',
                    'title'       => 'Lifetime 48" V-Frame Shatterproof Portable One Hand Height Adjustable Basketball System, 90585',
                    'price'       => 178.00,
                    'description' => 
                        "Now you, your family and your friends can shoot hoops whenever you would like out on the driveway with the Lifetime 48\" V-Frame Portable Basketball System. It is made of durable materials that can stand up to the elements as well as the everyday wear and tear of an intense game. This product features a 48\" v-frame steel backboard that is strong and durable. The Lifetime portable basketball system also has a heavy-duty arm height adjustment mechanism that moves from 8' to 10' high so that you can customize your experience. It includes a slam-it rim with an all-weather nylon net and a 3\" diameter round steel pole. This adjustable basketball system is sturdy and features a 31-gal portable base, which can be filled with water or sand to provide stability and anchoring. Bring the court to your home using this versatile piece of equipment. 
                        Lifetime 48\" V-Frame Portable Basketball System: 
                        Features a 48\" v-frame backboard (black) with frame pad
                        Strong arm adjustment
                        3\" round pole
                        3-piece Lifetime portable basketball system (black)
                        Courtside heavy-duty portable base, 31-gal capacity
                        Slam-it rim (black)
                        5-year limited warranty",
                    'on_sale'     => 1,
                    'num_reviews' => 17,
                    'category'    => null,
                    'images'      => 'https://i5.walmartimages.com/asr/e1f433f2-2fda-40be-8eb8-0ea3f4242010_1.a5d56181fe26ddf543e40dab8a11780e.jpeg?odnHeight=450&odnWidth=450&odnBg=FFFFFF',
                ]
            ]
        ];
    }
}