<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDifferentialsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('differentials', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('upc_id')->unsigned();
            $table->integer('product_id')->unsigned();
            $table->integer('listing_id')->unsigned();
            $table->float('difference', 10, 2);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('differentials');
    }
}
