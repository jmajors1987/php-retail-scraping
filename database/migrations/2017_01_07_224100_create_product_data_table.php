<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductDataTable extends Migration
{
    /**
     * 
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->string('store');
            $table->string('title', 1000);
            $table->float('price', 10, 2);
            $table->string('description', 5000)->nullable();
            $table->boolean('on_sale');
            $table->string('category')->nullable();
            $table->integer('num_reviews')->unsigned()->nullable();
            $table->string('images', 1000);
            $table->string('url', 1000);
            $table->integer('upc_id')->unsigned()->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
