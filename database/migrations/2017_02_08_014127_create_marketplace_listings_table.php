<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMarketplaceListingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('marketplace_listings', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->float('lowest_price', 10, 2)->nullable();
            $table->float('median_price', 10, 2)->nullable();
            $table->integer('total_for_sale')->unsigned()->nullable();
            $table->integer('offers')->unsigned()->nullable(); // I don't really understand this
            $table->integer('sales_rank')->nullable();
            $table->string('marketplace');
            $table->integer('upc_id')->unsigned();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('marketplace_listings');
    }
}
