<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (App::environment() == 'production') {
            exit("Don't run the seeder in production!");
        }

        $this->call(UsersTableSeeder::class);
    }
}
